This is the README file for the TAG1 group project - CS370 Fall 17.
This file will feauture documentation about the code base including 
a feature description, planned additions, installation instructions,
and general development information.
*********************************************************************
FEATURE DESCRIPTION
*********************************************************************
-Moddable Map: A level that is generated and rendered based on a 
 text file holding integer values 0-11
-Character Sprite: A unique sprite that represents the players 
 position on the map
-Round Timer:A timer that opens within the game window when the tab 
 key is pressed as a semi-transparent overlay to the game
-Scoreboard: Part of the timer, keeps track of player info, score, 
 and who is "it"
-Visibility Cone: A cone of light that is projected from the player 
 which illuminates parts of the map
-Directional view based on mouse position: Allows the user to control
 the cone of vision with the mouse to look around for other players
-Collision Based Tagging: Multiple players can be seen on the screen 
 and can collide with each other to create tag events
-Sound effects: In game music and a tag event based sound effect 
 have been added to the game
-Limited field of view: Shadows have been addede to the game based 
 on player position that prevent the player from being able to see 
 through walls
-Main Menu: Allows the player to start the game from a menu by 
 pressing a button
-Pause Menu: Allows the player to stop and resume gameplay while 
 in-game
-Artificial Intelligence: The AI controlled players will chase and 
 run from the player based on position of each
-Random Level Generation: A seperate piece of code can be ran to 
 generate a random map for the user to play on
*********************************************************************
PLANNED ADDITIONS
*********************************************************************
-Randomly Generated Levels
-User Picked Levels 
-User Made Levels 
-Distance Based Footsteps 
-Power-ups (Speed Boost, Untaggable) 
-Stat tracking
*********************************************************************
INSTALLATION INSTRUCTIONS
*********************************************************************














********************************************************************
DEV. NOTES / VERSION DESC.
********************************************************************
Tag 1 Game - Version 0.0.3
-This version adds a pause menu, performance improvements, Artificial
 Intelligence, and is representative of an MVP or minimum
 viable product. This version allows AI controlled players to be 
 spawned on the playfield and thus a complete game can be played in 
 which the player can tag and be tagged by the computer players, 
 score can be accumulated by all players and at the end of the alloted
 time a winner can be chosen.

 












