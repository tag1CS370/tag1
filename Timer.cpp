#include "Header.h"

Timer::Timer(SDL_Window* passedWindow, SDL_Renderer* passedRenderer) {
	timerScreenWidth = 250;
	timerScreenHeight = 85;
	isRunning = false;
	finished = false;
	shown = false;

	textColor = { 255, 240, 0, 255 };
	selectedTextColor = { 255, 0, 0, 255 };
	timeLimit = 900000;

	gameWindow = NULL;
	gameRenderer = NULL;
	timerFont = NULL;
	tTexture = NULL;

	gameRenderer = passedRenderer;
	gameWindow = passedWindow;

	timerFont = TTF_OpenFont("OpenSans-Regular.ttf", 28);

}

Timer::~Timer() {
	kill();

}

void Timer::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_TAB:
			shown = true;
			break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_TAB:
			shown = false;
			break;
		}
	}
}

void Timer::updateTimer() {
	if (SDL_GetTicks() >= (startTime + timeLimit - 2000)) {
		if (isRunning) {
			stopTimer();
			finished = true;
			for (int i = 0; i < players.size(); i++) {
				if (players[i].getIt()) {
					players[i].setScore((lastTagTime - players[i].getScore()) / 1000);
				}
				else {
					players[i].setScore((timeLimit - (timeLimit - SDL_GetTicks()) - players[i].getScore()) / 1000);
				}
			}
		}
	}
	if (shown) {
		SDL_Rect scoreboardBG = { 20, 20, (SCREEN_WIDTH - 40), (SCREEN_HEIGHT - 40) };
		SDL_Rect separator = { 20, 65, (SCREEN_WIDTH - 40), 2 };
		SDL_SetRenderDrawBlendMode(gameRenderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(gameRenderer, 0x00, 0x00, 0x00, 0x90);
		SDL_RenderFillRect(gameRenderer, &scoreboardBG);
		SDL_SetRenderDrawColor(gameRenderer, 0xFF, 0xB0, 0x00, 0xFF);
		SDL_RenderDrawRect(gameRenderer, &scoreboardBG);
		SDL_RenderDrawRect(gameRenderer, &separator);
		if (isRunning) {

			SDL_SetRenderDrawColor(gameRenderer, 0xFF, 0xFF, 0xFF, 0xFF);


			textString.str("");
			textString << "Time Left: " << ((timeLimit - SDL_GetTicks()) / 1000) / 60 << ":" << setw(2) << setfill('0') << ((timeLimit - SDL_GetTicks()) / 1000) % 60;

			if (tTexture != NULL) {
				SDL_DestroyTexture(tTexture);
				tTexture = NULL;
			}

			SDL_Surface* textSurface = TTF_RenderText_Solid(timerFont, textString.str().c_str(), textColor);
			tTexture = SDL_CreateTextureFromSurface(gameRenderer, textSurface);
			texW = textSurface->w;
			texH = textSurface->h;
			SDL_FreeSurface(textSurface);
			SDL_Rect renderQuad = { 30, 25, texW, texH };
			SDL_RenderCopyEx(gameRenderer, tTexture, NULL, &renderQuad, 0.0, NULL, SDL_FLIP_NONE);

			for (int i = 0; i < players.size(); i++) {
				textString.str("");

				if (players[i].getIt()) {
					textString << players[i].getPlayerName() << ": " << (lastTagTime - players[i].getScore()) / 1000;
				}
				else {
					textString << players[i].getPlayerName() << ": " << (timeLimit - (timeLimit - SDL_GetTicks()) - players[i].getScore()) / 1000;
				}

				if (tTexture != NULL) {
					SDL_DestroyTexture(tTexture);
					tTexture = NULL;
				}

				SDL_Surface* textSurface = TTF_RenderText_Solid(timerFont, textString.str().c_str(), textColor);

				if (players[i].getIt()) {
					textSurface = TTF_RenderText_Solid(timerFont, textString.str().c_str(), selectedTextColor);
				}

				tTexture = SDL_CreateTextureFromSurface(gameRenderer, textSurface);
				texW = textSurface->w;
				texH = textSurface->h;
				SDL_FreeSurface(textSurface);
				renderQuad = { 30, (65 + 40 * i), texW, texH };
				SDL_RenderCopyEx(gameRenderer, tTexture, NULL, &renderQuad, 0.0, NULL, SDL_FLIP_NONE);

			}
		}
		else {
			textString.str("");
			textString << "Time Left: Done";

			if (tTexture != NULL) {
				SDL_DestroyTexture(tTexture);
				tTexture = NULL;
			}

			SDL_Surface* textSurface = TTF_RenderText_Solid(timerFont, textString.str().c_str(), textColor);
			tTexture = SDL_CreateTextureFromSurface(gameRenderer, textSurface);
			texW = textSurface->w;
			texH = textSurface->h;
			SDL_FreeSurface(textSurface);
			SDL_Rect renderQuad = { 30, 25, texW, texH };
			SDL_RenderCopyEx(gameRenderer, tTexture, NULL, &renderQuad, 0.0, NULL, SDL_FLIP_NONE);

			for (int i = 0; i < players.size(); i++) {
				textString.str("");
				textString << players[i].getPlayerName() << ": " << players[i].getScore();

				if (tTexture != NULL) {
					SDL_DestroyTexture(tTexture);
					tTexture = NULL;
				}

				SDL_Surface* textSurface = TTF_RenderText_Solid(timerFont, textString.str().c_str(), textColor);
				tTexture = SDL_CreateTextureFromSurface(gameRenderer, textSurface);
				texW = textSurface->w;
				texH = textSurface->h;
				SDL_FreeSurface(textSurface);
				renderQuad = { 30, (65 + 40 * i), texW, texH };
				SDL_RenderCopyEx(gameRenderer, tTexture, NULL, &renderQuad, 0.0, NULL, SDL_FLIP_NONE);

			}
		}
	}
}


void Timer::kill() {
	if (tTexture != NULL) {
		SDL_DestroyTexture(tTexture);
		tTexture = NULL;
	}

	TTF_CloseFont(timerFont);
	timerFont = NULL;

}


int Timer::getPlayerCount() {
	return players.size();

}

bool Timer::getRunning() {
	return isRunning;

}
int Timer::getTimerTime() {
	return SDL_GetTicks() - startTime;

}

void Timer::setTimeLimit(int newTime) {
	timeLimit = newTime;

}

int Timer::getTimeLimit() {
	return timeLimit;

}

void Timer::setTimerScreenWidth(int newWidth) {
	timerScreenWidth = newWidth;

}

void Timer::setTimerScreenHeight(int newHeight) {
	timerScreenHeight = newHeight;

}

int Timer::getTimerScreenWidth() {
	return timerScreenWidth;

}

int Timer::getTimerScreenHeight() {
	return timerScreenHeight;

}

void Timer::addPlayer(string newPlayer) {
	players.push_back(newPlayer);

	if (players.size() == 1) {
		players[0].setControlled(true);
	}

}

bool Timer::removePlayer(string remPlayer) {
	bool success = true;

	if (players.size() != 0) {
		for (int i = 0; i < players.size(); i++) {
			if (players[i].getPlayerName().compare(remPlayer) == 0) {
				players.erase(players.begin() + i);

				break;
			}

			if (i == players.size() - 1) {
				success = false;
			}
		}
	}
	else {
		success = false;
	}

	return success;

}

void Timer::startTimer() {
	startTime = SDL_GetTicks();
	stopTime = SDL_GetTicks();
	isRunning = true;
	finished = false;

}

void Timer::stopTimer() {
	stopTime = SDL_GetTicks();
	isRunning = false;

}

int Timer::getPID(string username) {
	int ID = -1;
	if (players.size() != 0) {
		for (int i = 0; i < players.size(); i++) {
			if (players[i].getPlayerName().compare(username) == 0) {
				ID = i;
			}
		}
	}
	return ID;
}

void Timer::newTag(string player) {
	for (int i = 0; i < players.size(); i++) {
		if (players[i].getIt()) {
			players[i].setIt(false);
			players[i].addScore((SDL_GetTicks() - lastTagTime));
		}
		else if (players[i].getPlayerName().compare(player) == 0) {
			players[i].setIt(true);
		}

	}
	lastTagTime = SDL_GetTicks();

}

int Timer::getLastTagTime() {
	return lastTagTime;
}

vector<Player> Timer::getPlayerList() {
	return players;
}