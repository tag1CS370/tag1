#include "Header.h"

playerAI::playerAI(vector<Player>* passedPlayers, int index) {
	playerList = passedPlayers;
	controlledIndex = index;

	XMove = 0;
	YMove = 0;

	moveExceptionX = 0;
	moveExceptionY = 0;

	goalXMove = 0;
	goalYMove = 0;

	ExCountX = 0;
	ExCountY = 0;
	exceptionCount = 0;

}

void playerAI::think() {
	vector<int> playersDistance;
	vector<double> playerAngles;

	seenIndexes.clear();

	for (int i = 0; i < playerList->size() - 1; i++) {
		if (i == controlledIndex) {
			playersDistance.push_back(-1);
			playerAngles.push_back(-1);
			continue;
		}

		int x = pow(((*playerList)[i].getXCoord() + 10) - ((*playerList)[controlledIndex].getXCoord() + 10), 2);
		int y = pow(((*playerList)[i].getYCoord() + 10) - ((*playerList)[controlledIndex].getYCoord() + 10), 2);
		int dist = sqrt(x + y);
		playersDistance.push_back(dist);

		double directAngle = (180.f / 3.14159265f) * atan2((int)((*playerList)[i].getYCoord()) - ((*playerList)[controlledIndex].getYCoord() + 10), (((*playerList)[i].getXCoord())) - ((*playerList)[controlledIndex].getXCoord() + 10));

		bool isInVision = false;
		int halfViewCone = 40;

		if (directAngle > (*playerList)[controlledIndex].getViewAngle() - halfViewCone && directAngle < (*playerList)[controlledIndex].getViewAngle() + halfViewCone) {
			isInVision = true;
		}

		if ((*playerList)[controlledIndex].getViewAngle() + halfViewCone > 180) {
			if (directAngle > (*playerList)[controlledIndex].getViewAngle() - halfViewCone || directAngle < -180 + (((*playerList)[controlledIndex].getViewAngle() + halfViewCone) -180)) {
				isInVision = true;
			}
		}

		if ((*playerList)[controlledIndex].getViewAngle() - halfViewCone < -180) {
			if (directAngle > (180 + ((*playerList)[controlledIndex].getViewAngle() - halfViewCone) + 180) || directAngle < (*playerList)[controlledIndex].getViewAngle() + halfViewCone) {
				isInVision = true;
			}
		}

		if (dist < 60)
			isInVision = true;

		if (dist < 400 && isInVision) {
			seenIndexes.push_back(i);
		}
	}

	int closestIndex = -1;
	int closestDist = 9999999;

	for (int j = 0; j < seenIndexes.size(); j++) {
		if (playersDistance[seenIndexes[j]] < closestDist) {
			closestDist = playersDistance[seenIndexes[j]];
			closestIndex = seenIndexes[j];
		}
	}

	if (closestIndex != -1) {
		if ((*playerList)[controlledIndex].getIt()) {
			targetIndex = closestIndex;
			currentAction = chase;
		}
		else {
			for (int k = 0; k < playerList->size(); k++) {
				if ((*playerList)[k].getIt()) {
					targetIndex = k;
					currentAction = runAway;
					break;
				}
				else {
					currentAction = wander;
				}
			}
		}
	}
	else {
		currentAction = wander;
	}
}

void playerAI::processAI() {
	double targetDir = (180.f / 3.14159265f) * atan2((int)((*playerList)[targetIndex].getYCoord() + 10) - ((*playerList)[controlledIndex].getYCoord() + 10), (((*playerList)[targetIndex].getXCoord() + 10)) - ((*playerList)[controlledIndex].getXCoord() + 10));
	
	int x = pow(((*playerList)[targetIndex].getXCoord() + 10) - ((*playerList)[controlledIndex].getXCoord() + 10), 2);
	int y = pow(((*playerList)[targetIndex].getYCoord() + 10) - ((*playerList)[controlledIndex].getYCoord() + 10), 2);
	int targetDist = sqrt(x + y);

	switch (currentAction) {
	case chase:
		(*playerList)[controlledIndex].setViewAngle(targetDir);

		if ((*playerList)[targetIndex].getYCoord() < (*playerList)[controlledIndex].getYCoord() - 15) {
			(*playerList)[controlledIndex].setYForce(-1);
		}
		else if ((*playerList)[targetIndex].getYCoord() > (*playerList)[controlledIndex].getYCoord() + 15) {
			(*playerList)[controlledIndex].setYForce(1);
		}
		else {
			(*playerList)[controlledIndex].setYForce(0);
		}

		if ((*playerList)[targetIndex].getXCoord() < (*playerList)[controlledIndex].getXCoord() - 15) {
			(*playerList)[controlledIndex].setXForce(-1);
		}
		else if ((*playerList)[targetIndex].getXCoord() > (*playerList)[controlledIndex].getXCoord() + 15) {
			(*playerList)[controlledIndex].setXForce(1);
		}
		else {
			(*playerList)[controlledIndex].setXForce(0);
		}

		//printf("%i Chasing %i\n", controlledIndex, targetIndex);
		break;

	case runAway:
		(*playerList)[controlledIndex].setViewAngle((180.f / 3.14159265f) * atan2((int)((*playerList)[targetIndex].getYCoord() + 10) - ((*playerList)[controlledIndex].getYCoord() + 10), (((*playerList)[targetIndex].getXCoord() + 10)) - ((*playerList)[controlledIndex].getXCoord() + 10)));

		if (exceptionCount < 3) {
			if ((*playerList)[targetIndex].getYCoord() < (*playerList)[controlledIndex].getYCoord() - 15) {
				(*playerList)[controlledIndex].setYForce(1);
			}
			else if ((*playerList)[targetIndex].getYCoord() > (*playerList)[controlledIndex].getYCoord() + 15) {
				(*playerList)[controlledIndex].setYForce(-1);
			}
			else {
				(*playerList)[controlledIndex].setYForce(0);
			}

			if ((*playerList)[targetIndex].getXCoord() < (*playerList)[controlledIndex].getXCoord() - 15) {
				(*playerList)[controlledIndex].setXForce(1);
			}
			else if ((*playerList)[targetIndex].getXCoord() > (*playerList)[controlledIndex].getXCoord() + 15) {
				(*playerList)[controlledIndex].setXForce(-1);
			}
			else {
				(*playerList)[controlledIndex].setXForce(0);
			}

			//printf("%i , %i | %i , %i\n", (*playerList)[controlledIndex].getBox().x, goalXMove, (*playerList)[controlledIndex].getBox().y, goalYMove);

			if ((*playerList)[controlledIndex].getForceX() != 0 && (*playerList)[controlledIndex].getBox().x == lastX && moveExceptionX == 0) {
				//printf("X Exception detected ");
				exceptionCount++;
				switch (rand() % 2) {
				case 0:
					(*playerList)[controlledIndex].setYForce(1);
					moveExceptionY = 1;
					break;
				case 1:
					(*playerList)[controlledIndex].setYForce(-1);
					moveExceptionY = -1;
					break;
				}
			}
			else if ((*playerList)[controlledIndex].getForceY() && (*playerList)[controlledIndex].getBox().y == lastY && moveExceptionY == 0) {
				//printf("Y Exception detected");
				exceptionCount++;
				switch (rand() % 2) {
				case 0:
					(*playerList)[controlledIndex].setXForce(1);
					moveExceptionX = 1;
					break;
				case 1:
					(*playerList)[controlledIndex].setXForce(-1);
					moveExceptionX = -1;
					break;
				}
			}
			else {
				exceptionCount = 0;
			}
		}
		else {
			//printf("MOVE EXCEPTION | %i %i |\n", moveExceptionX, moveExceptionY);

			(*playerList)[controlledIndex].setXForce(moveExceptionX);
			(*playerList)[controlledIndex].setYForce(moveExceptionY);
			
			if ((*playerList)[controlledIndex].getBox().x == lastX && moveExceptionX == 0) {
				ExCountX++;
				if (ExCountX > 15) {
					if (moveExceptionX == -1) {
						moveExceptionX = 1;
						ExCountX = 0;
					}
					else {
						moveExceptionX = -1;
						ExCountX = 0;
					}
				}
			}
			if ((*playerList)[controlledIndex].getBox().y == lastY && moveExceptionY == 0) {
				ExCountY++;
				if (ExCountY > 15) {
					if (moveExceptionY == -1) {
						moveExceptionY = 1;
						ExCountY = 0;
					}
					else {
						moveExceptionY = -1;
						ExCountY = 0;
					}
				}
			}

			if (moveExceptionX != 0 && (*playerList)[controlledIndex].getBox().y != lastY) {
				moveExceptionX = 0;
				exceptionCount = 0;
				ExCountX = 0;
			}
			if (moveExceptionY != 0 && (*playerList)[controlledIndex].getBox().x != lastX) {
				moveExceptionY = 0;
				exceptionCount = 0;
				ExCountY = 0;
			}
		}

		lastX = (*playerList)[controlledIndex].getBox().x;
		lastY = (*playerList)[controlledIndex].getBox().y;

		//printf("%i Running from %i\n", controlledIndex, targetIndex);
		break;

	case wander:
	default:
		moveExceptionX = 0;
		moveExceptionY = 0;
		exceptionCount = 0;
		if ((*playerList)[controlledIndex].getXVelocity() == 0 && goalXMove != 0) {
			switch (goalXMove) {
			case -1:
				(*playerList)[controlledIndex].setXForce(1);
				goalXMove = 1;
				break;
			case 1:
				(*playerList)[controlledIndex].setXForce(-1);
				goalXMove = -1;
				break;
			}
			if (goalYMove == 0) {
				switch (rand() % 2) {
				case 0:
					(*playerList)[controlledIndex].setYForce(1);
					break;
				case 1:
					(*playerList)[controlledIndex].setYForce(-1);
					break;
				}
			}
		}

		if ((*playerList)[controlledIndex].getYVelocity() == 0 && goalYMove != 0) {
			switch (goalXMove) {
			case -1:
				(*playerList)[controlledIndex].setXForce(1);
				goalXMove = 1;
				break;
			case 1:
				(*playerList)[controlledIndex].setXForce(-1);
				goalXMove = -1;
				break;
			}
			if (goalXMove == 0) {
				switch (rand() % 2) {
				case 0:
					(*playerList)[controlledIndex].setXForce(1);
					break;
				case 1:
					(*playerList)[controlledIndex].setXForce(-1);
					break;
				}
			}
		}

		if ((*playerList)[controlledIndex].getXVelocity() == 0 && goalXMove == 0) {
			switch (rand() % 2) {
			case 0:
				(*playerList)[controlledIndex].setXForce(1);
				break;
			case 1:
				(*playerList)[controlledIndex].setXForce(-1);
				break;
			}
		}

		if ((*playerList)[controlledIndex].getYVelocity() == 0 && goalYMove == 0) {
			switch (rand() % 2) {
			case 0:
				(*playerList)[controlledIndex].setYForce(1);
				break;
			case 1:
				(*playerList)[controlledIndex].setYForce(-1);
				break;
			}
		}

		if ((*playerList)[controlledIndex].getForceX() == 0) {
			switch (rand() % 2) {
			case 0:
				(*playerList)[controlledIndex].setXForce(1);
				break;
			case 1:
				(*playerList)[controlledIndex].setXForce(-1);
				break;
			}
		}
		if ((*playerList)[controlledIndex].getForceY() == 0) {
			switch (rand() % 2) {
			case 0:
				(*playerList)[controlledIndex].setYForce(1);
				break;
			case 1:
				(*playerList)[controlledIndex].setYForce(-1);
				break;
			}
		}

		(*playerList)[controlledIndex].setViewAngle((180.f / 3.14159265f) * atan2((*playerList)[controlledIndex].getForceY() * -1, (*playerList)[controlledIndex].getForceX()));

		/*
		if ((*playerList)[controlledIndex].getForceX() == 1 && (*playerList)[controlledIndex].getForceY() == 0) {
			(*playerList)[controlledIndex].setViewAngle(180);
		}
		else if ((*playerList)[controlledIndex].getForceX() == 1 && (*playerList)[controlledIndex].getForceY() == 1) {
			(*playerList)[controlledIndex].setViewAngle(135);
		}
		else if ((*playerList)[controlledIndex].getForceX() == 0 && (*playerList)[controlledIndex].getForceY() == 1) {
			(*playerList)[controlledIndex].setViewAngle(90);
		}
		else if ((*playerList)[controlledIndex].getForceX() == -1 && (*playerList)[controlledIndex].getForceY() == 1) {
			(*playerList)[controlledIndex].setViewAngle(45);
		}
		else if ((*playerList)[controlledIndex].getForceX() == -1 && (*playerList)[controlledIndex].getForceY() == 0) {
			(*playerList)[controlledIndex].setViewAngle(0);
		}
		else if ((*playerList)[controlledIndex].getForceX() == -1 && (*playerList)[controlledIndex].getForceY() == -1) {
			(*playerList)[controlledIndex].setViewAngle(-45);
		}
		else if ((*playerList)[controlledIndex].getForceX() == 0 && (*playerList)[controlledIndex].getForceY() == -1) {
			(*playerList)[controlledIndex].setViewAngle(-90);
		}
		else if ((*playerList)[controlledIndex].getForceX() == 1 && (*playerList)[controlledIndex].getForceY() == -1) {
			(*playerList)[controlledIndex].setViewAngle(135);
		}
		*/

		//printf("%i Wandering\n", controlledIndex);
		break;
	}
}