#include <SDL.h>
#undef main
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <SDL2_gfxPrimitives.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>

using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

//The dimensions of the level
const int LEVEL_WIDTH = 1280;
const int LEVEL_HEIGHT = 960;

//Tile constants
const int TILE_WIDTH = 80;
const int TILE_HEIGHT = 80;
const int TOTAL_TILES = 192;
const int TOTAL_TILE_SPRITES = 12;

//The different tile sprites
const int TILE_RED = 0;
const int TILE_GREEN = 1;
const int TILE_BLUE = 2;
const int TILE_CENTER = 3;
const int TILE_TOP = 4;
const int TILE_TOPRIGHT = 5;
const int TILE_RIGHT = 6;
const int TILE_BOTTOMRIGHT = 7;
const int TILE_BOTTOM = 8;
const int TILE_BOTTOMLEFT = 9;
const int TILE_LEFT = 10;
const int TILE_TOPLEFT = 11;

enum AIaction { chase, runAway, wander };
enum AIdirection { up, down, left, right, upLeft, upRight, downLeft, downRight, neutral };

//Texture wrapper class
class LTexture
{
public:
	//Initializes variables
	LTexture();

	//Deallocates memory
	~LTexture();

	//Loads image at specified path
	bool loadFromFile(std::string path);

#ifdef _SDL_TTF_H
	//Creates image from font string
	bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
#endif

	//Deallocates texture
	void free();

	//Set color modulation
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void setBlendMode(SDL_BlendMode blending);

	//Set alpha modulation
	void setAlpha(Uint8 alpha);

	//Renders texture at given point
	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Gets image dimensions
	int getWidth();
	int getHeight();

private:
	//The actual hardware texture
	SDL_Texture* mTexture;

	//Image dimensions
	int mWidth;
	int mHeight;
};

class Tile
{
public:
	//Initializes position and type
	Tile(int x, int y, int tileType);

	//Shows the tile
	void render(SDL_Rect& camera);

	//Get the tile type
	int getType();

	//Get the collision box
	SDL_Rect getBox();

private:
	//The attributes of the tile
	SDL_Rect mBox;

	//The tile type
	int mType;
};

class Timer;

class Player
{
	//Declaration of functions for player class
public:

	//Dimensions of player
	static const int PLAYER_WIDTH = 20;
	static const int PLAYER_HEIGHT = 20;

	//velocity of player
	int PLAYER_MAX_VEL = 8;
	float PLAYER_ACCEL = 0.4f;

	Player();
	Player(string name);
	Player(int xCoord, int yCoord);
	Player(int xCoord, int yCoord, string name);
	void setXCoord(int newX);
	int getXCoord();
	void setYCoord(int newY);
	int getYCoord();
	void setVis(bool visible);
	bool getVis();
	void setScore(int newScore);
	void addScore(int addedScore);
	int getScore();
	void setIt(bool it);
	bool getIt();
	void setSpeedMultiplier(float newMult);
	float getSpeedMultiplier();
	void setPlayerName(string newName);
	string getPlayerName();
	int getPID();
	void setPID(int PIDnum);
	void handleEvent(SDL_Event& e);
	void move(Tile *tiles[], Timer& currentTImer);
	void setCamera(SDL_Rect& camera);
	void render(SDL_Rect& camera);
	void updatePos();
	SDL_Rect getBox();
	void setControlled(bool control);
	bool getControlled();
	double getViewAngle();
	void setViewAngle(double passedViewAngle);
	void setXForce(int XDir);
	void setYForce(int YDir);
	float getXVelocity();
	float getYVelocity();
	float getForceX();
	float getForceY();
	void playFootstep();

	//Declaration of variables for player class
private:
	int playerXCoord = 0;
	int playerYCoord = 0;
	bool isVisible = false;
	int score = 0;
	bool isIt = false;
	float speedMultiplier = 1.0;
	string playerName = "player";
	int PIDnumber = NULL;
	//Collision box of the player
	SDL_Rect mBox;
	//The velocity of the player
	float velX, velY, forceX, forceY;
	float mVelX, mVelY;
	double viewAngle = 0.0;
	int lastStepX = 0;
	int lastStepY = 0;
	bool isControlled = false;

};

class tank : Player
{
	//Declaration of functions for player class
public:

	//velocity of player
	static const int PLAYER_VEL = 2;
	static const int PLAYER_REV = -1;
	float PLAYER_ACCEL = 0.1f;
	int TURN_SPEED = 3;

	tank();
	tank(string name);
	tank(int xCoord, int yCoord);
	tank(int xCoord, int yCoord, string name);
	void handleEvent(SDL_Event& e);
	void move(Tile *tiles[]);
	void render(SDL_Rect& camera);

	//Declaration of variables for player class
private:
	float vel = 0;
	float hullAngle = 0;
	int turn = 0;
	float force = 0;

};

class Timer {
public:
	Timer(SDL_Window* passedWindow, SDL_Renderer* passedRenderer);
	~Timer();

	vector<Player> players;

	//Renders the timer, should be in the main loop
	void updateTimer();

	void handleEvent(SDL_Event& e);

	//Close out window, renderer, font, and texture
	void kill();

	//Returns length of playerList
	int getPlayerCount();

	bool getRunning();

	//Returns SDL_GetTicks - startTime
	int getTimerTime();

	//Sets time limit (in milliseconds)
	void setTimeLimit(int newTime);

	//Gets time limit (in milliseconds)
	int getTimeLimit();

	void setTimerScreenWidth(int newWidth);
	void setTimerScreenHeight(int newHeight);
	int getTimerScreenWidth();
	int getTimerScreenHeight();

	//Pushes input string to end of playerList
	void addPlayer(string newPlayer);

	//Searches playerList for input and removes if found
	bool removePlayer(string remPlayer);

	//Sets startTime, stopTime, and isRunning
	void startTimer();

	//Sets stopTime and isRunning
	void stopTimer();

	//Returns string position in playerList or -1 of not found
	int getPID(string username);

	//Sets input player's tag bool to true and old tagged to false
	void newTag(string player);

	int getLastTagTime();

	vector<Player> getPlayerList();

private:
	int timerScreenWidth;
	int timerScreenHeight;

	int gameWindowX;
	int gameWindowY;

	bool isRunning;
	int startTime;
	int stopTime;
	int lastTagTime;
	bool finished;
	bool shown;

	int timeLimit;

	SDL_Color textColor;
	SDL_Color selectedTextColor;

	SDL_Window* gameWindow;
	SDL_Renderer* gameRenderer;
	TTF_Font *timerFont;

	SDL_Texture* tTexture;
	stringstream textString;
	int texW;
	int texH;

};

class powerUp
{
private:

	//name, duration and number
	string powerUpName;
	int duration = 5217;
	int powerUpNum;

	//collision box for powerup
	SDL_Rect pBox;


public:
	//Dimensions of powerup
	static const int POWERUP_WIDTH = 20;
	static const int POWERUP_HEIGHT = 20;

	powerUp();
	powerUp(int number);
	int generatePUpNum();
	void choosePowerUp(int pUpNum);

};

class playerAI {
public:
	playerAI(vector<Player>* passedPlayers, int index);
	void think();
	void processAI();

private:
	Player controlledPlayer;
	AIaction currentAction;
	vector<Player>* playerList;
	int controlledIndex;
	vector<int> seenIndexes;
	int targetIndex;
	int YMove, XMove;
	int goalYMove, goalXMove;
	int moveExceptionX, moveExceptionY;
	int exceptionCount;
	int ExCountX, ExCountY;
	int lastX, lastY;

};

class Buttons {
public:
	Buttons();
	Buttons(int bBid);
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);
	void buttonRender();
	bool handleEvent(SDL_Event& e);
	void setButtonTexture();
	int onPress();
	SDL_Surface* buttonSurface;
private:
	int mousePosX;
	int mousePosY;
	int coordX;
	int coordY;
	int Bid;
};

//Starts up SDL and creates window
bool init();

//Loads media
bool loadGameMedia(Tile* tiles[]);

//Frees media and shuts down SDL
void closeGameMedia(Tile* tiles[]);

//Sets tiles from tile map
bool setTiles(Tile *tiles[]);

bool touchesWall(SDL_Rect box, Tile* tiles[]);

bool touchesPlayer(SDL_Rect box, Timer& currentTimer, int testPlayer);

bool checkCollision(SDL_Rect a, SDL_Rect b);