#include "Header.h"

/*
stringstream sysLogStream;
stringstream gameLogStream;
stringstream errorLogStream;

void addSysLog(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

sysLogStream << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";

}

void addGameLog(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

gameLogStream << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";

}

void addErrorLog(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

errorLogStream << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";

}

void addSysLogNow(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_syslog.txt";

ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << "***" << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";
logOutput.close();

}

void addGameLogNow(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_gamelog.txt";

ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << "***" << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";
logOutput.close();

}

void addErrorLogNow(string entry) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_errorlog.txt";

ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << "***" << (now.tm_hour) << ":" << (now.tm_min) << ":" << (now.tm_sec) << "|" << entry << "\n";
logOutput.close();

}

void writeSysLog() {
if (!(sysLogStream.str().empty())) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_syslog.txt";


ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << sysLogStream.str();
logOutput.close();

sysLogStream.str("");
}

}

void writeGameLog() {
if (!(gameLogStream.str().empty())) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_gamelog.txt";


ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << gameLogStream.str();
logOutput.close();

gameLogStream.str("");
}

}

void writeErrorLog() {
if (!(errorLogStream.str().empty())) {
time_t t = time(0);
struct tm now;
localtime_s(&now, &t);

stringstream logFile;
logFile.str("");
logFile << "logs/" << (now.tm_mon + 1) << "-" << (now.tm_mday) << "-" << (now.tm_year + 1900) << "_errorlog.txt";


ofstream logOutput;
logOutput.open(logFile.str(), ios_base::app);
logOutput << errorLogStream.str();
logOutput.close();

errorLogStream.str("");
}

}
*/


TTF_Font *gFont = NULL;
SDL_Color textColor = { 0, 0, 0, 255 };
SDL_Renderer* gRenderer = NULL;
SDL_Window* gWindow = NULL;
LTexture gPlayerTexture;
LTexture gPlayerMask;
LTexture gTileTexture;
LTexture gHullTexture;
LTexture gTurretTexture;
LTexture mainMenuBackgroundTile;
LTexture gGameLogo;
LTexture gGameTutorial;
SDL_Rect gTileClips[TOTAL_TILE_SPRITES];
Mix_Music *music = NULL;
Mix_Chunk *tagSound = NULL;
Mix_Chunk *gasBlast = NULL;
Mix_Chunk *Schlick = NULL;
Mix_Chunk *Step1 = NULL;
Mix_Chunk *Step2 = NULL;
Mix_Chunk *Step3 = NULL;
Mix_Chunk *Step4 = NULL;


LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile(std::string path)
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if (textSurface != NULL)
	{
		//Create texture from surface pixels
		mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
		if (mTexture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}
	else
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}


	//Return success
	return mTexture != NULL;
}
#endif

void LTexture::free()
{
	//Free texture if it exists
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void LTexture::setBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void LTexture::setAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}


Tile::Tile(int x, int y, int tileType)
{
	//Get the offsets
	mBox.x = x;
	mBox.y = y;

	//Set the collision box
	mBox.w = TILE_WIDTH;
	mBox.h = TILE_HEIGHT;

	//Get the tile type
	mType = tileType;
}

void Tile::render(SDL_Rect& camera)
{
	//Show the tile
	gTileTexture.render(mBox.x - camera.x, mBox.y - camera.y, &gTileClips[mType]);
}

int Tile::getType()
{
	return mType;
}

SDL_Rect Tile::getBox()
{
	return mBox;
}

Player::Player() //No-arg constructor
{
	//Initialize the collision box
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = PLAYER_WIDTH;
	mBox.h = PLAYER_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	forceX = 0;
	forceY = 0;
}

Player::Player(string name) //arg constructor
{
	playerName = name;
	//Initialize the collision box
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = PLAYER_WIDTH;
	mBox.h = PLAYER_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	forceX = 0;
	forceY = 0;
}

Player::Player(int xCoord, int yCoord) //Constructor that takes position in x and y coords
{
	playerXCoord = xCoord;
	playerYCoord = yCoord;

	//Initialize the collision box
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = PLAYER_WIDTH;
	mBox.h = PLAYER_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	forceX = 0;
	forceY = 0;

	setXCoord(playerXCoord);
	setYCoord(playerYCoord);
}

Player::Player(int xCoord, int yCoord, string name) //Constructor that takes position in x and y coords
{
	playerName = name;
	playerXCoord = xCoord;
	playerYCoord = yCoord;

	//Initialize the collision box
	mBox.x = 0;
	mBox.y = 0;
	mBox.w = PLAYER_WIDTH;
	mBox.h = PLAYER_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	forceX = 0;
	forceY = 0;

	setXCoord(playerXCoord);
	setYCoord(playerYCoord);
}

void Player::setXCoord(int newX)
{
	mBox.x = newX;
}

int Player::getXCoord()
{
	return mBox.x;
}

void Player::setYCoord(int newY)
{
	mBox.y = newY;
}

int Player::getYCoord()
{
	return mBox.y;
}

void Player::setVis(bool visible)
{
	isVisible = visible;
}

bool Player::getVis()
{
	return isVisible;
}

void Player::setScore(int newScore)
{
	score = newScore;
}

void Player::addScore(int addedScore)
{
	score += addedScore;
}
int Player::getScore()
{
	return score;
}

void Player::setIt(bool it)
{
	isIt = it;
}

bool Player::getIt()
{
	return isIt;
}

void Player::setSpeedMultiplier(float newMult)
{
	speedMultiplier = newMult;
}

float Player::getSpeedMultiplier()
{
	return speedMultiplier;
}

void Player::setPlayerName(string newName)
{
	playerName = newName;
}

string Player::getPlayerName()
{
	return playerName;
}

int Player::getPID()
{
	return PIDnumber;
}

void Player::setPID(int PIDnum)
{
	PIDnumber = PIDnum;
}

void Player::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_w: forceY -= (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_s: forceY += (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_a: forceX -= (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_d: forceX += (float)PLAYER_ACCEL * speedMultiplier; break;
			//		case SDLK_LCTRL: PLAYER_MAX_VEL -= 4; break;
			//		case SDLK_LSHIFT: PLAYER_MAX_VEL += 4; break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_w: forceY += (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_s: forceY -= (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_a: forceX += (float)PLAYER_ACCEL * speedMultiplier; break;
		case SDLK_d: forceX -= (float)PLAYER_ACCEL * speedMultiplier; break;
			//		case SDLK_LCTRL: PLAYER_MAX_VEL += 4; break;
			//		case SDLK_LSHIFT: PLAYER_MAX_VEL -= 4; break;
		}
	}
}

void Player::move(Tile *tiles[], Timer& currentTimer)
{
	velX += forceX;
	if (velX > PLAYER_MAX_VEL)
		velX = PLAYER_MAX_VEL;
	if (velX < -PLAYER_MAX_VEL)
		velX = -PLAYER_MAX_VEL;

	velY += forceY;
	if (velY > PLAYER_MAX_VEL)
		velY = PLAYER_MAX_VEL;
	if (velY < -PLAYER_MAX_VEL)
		velY = -PLAYER_MAX_VEL;

	if (forceX == 0.0 && velX > 0.0)
		velX -= 0.35f;
	else if (forceX == 0.0 && velX < 0.0)
		velX += 0.35f;
	if (forceY == 0.0 && velY > 0.0)
		velY -= 0.35f;
	else if (forceY == 0.0 && velY < 0.0)
		velY += 0.35f;

	//Move the player left or right
	mBox.x += (int)velX;

	//If the player went too far to the left or right or touched a wall
	if ((mBox.x < 0) || (mBox.x + PLAYER_WIDTH > LEVEL_WIDTH) || touchesWall(mBox, tiles) || touchesPlayer(mBox, currentTimer, PIDnumber))
	{
		//move back
		mBox.x -= (int)velX;
		velX = 0;
	}

	//Move the player up or down
	mBox.y += (int)velY;

	//If the player went too far up or down or touched a wall
	if ((mBox.y < 0) || (mBox.y + PLAYER_HEIGHT > LEVEL_HEIGHT) || touchesWall(mBox, tiles) || touchesPlayer(mBox, currentTimer, PIDnumber))
	{
		//move back
		mBox.y -= (int)velY;
		velY = 0;
	}

	int distance = 0;
	distance = sqrt(pow((lastStepX - mBox.x), 2) + pow((lastStepY - mBox.y), 2));

	if (distance > 125) {
		playFootstep();
	}
}

void Player::setCamera(SDL_Rect& camera)
{
	//Center the camera over the player
	camera.x = (mBox.x + PLAYER_WIDTH / 2) - SCREEN_WIDTH / 2;
	camera.y = (mBox.y + PLAYER_HEIGHT / 2) - SCREEN_HEIGHT / 2;

	//Keep the camera in bounds
	if (camera.x < 0)
	{
		camera.x = 0;
	}
	if (camera.y < 0)
	{
		camera.y = 0;
	}
	if (camera.x > LEVEL_WIDTH - camera.w)
	{
		camera.x = LEVEL_WIDTH - camera.w;
	}
	if (camera.y > LEVEL_HEIGHT - camera.h)
	{
		camera.y = LEVEL_HEIGHT - camera.h;
	}
}

void Player::render(SDL_Rect& camera)
{
	int xMouse;
	int yMouse;

	if (isControlled)
	{
		SDL_GetMouseState(&xMouse, &yMouse);
		viewAngle = (180.0000 / 3.1415) * atan2(yMouse - (mBox.y + 10 - camera.y), xMouse - (mBox.x + 10 - camera.x));

		gPlayerMask.render(mBox.x - camera.x - 2490, mBox.y - camera.y - 2490, NULL, viewAngle);
	}

	if (viewAngle > 90 || viewAngle < -90) {
		gPlayerTexture.render(mBox.x - camera.x, mBox.y - camera.y, NULL, viewAngle, NULL, SDL_FLIP_VERTICAL);
	}
	else {
		gPlayerTexture.render(mBox.x - camera.x, mBox.y - camera.y, NULL, viewAngle);
	}

}

void Player::updatePos()
{
	mBox.x = playerXCoord;
	mBox.y = playerYCoord;

}

SDL_Rect Player::getBox()
{
	return mBox;
}

void Player::setControlled(bool control)
{
	isControlled = control;

}

bool Player::getControlled()
{
	return isControlled;

}

double Player::getViewAngle()
{
	return viewAngle;

}

void Player::setViewAngle(double passedViewAngle)
{
	viewAngle = passedViewAngle;

}

void Player::setXForce(int XDir) {
	switch (XDir) {
	case -1:
		forceX = -((float)PLAYER_ACCEL * speedMultiplier);
		break;
	case 0:
		forceX = 0;
		break;
	case 1:
		forceX = (float)PLAYER_ACCEL * speedMultiplier;
		break;
	}
}

void Player::setYForce(int YDir) {
	switch (YDir) {
	case -1:
		forceY = -((float)PLAYER_ACCEL * speedMultiplier);
		break;
	case 0:
		forceY = 0;
		break;
	case 1:
		forceY = (float)PLAYER_ACCEL * speedMultiplier;
		break;
	}
}

float Player::getXVelocity() {
	return velX;
}

float Player::getYVelocity() {
	return velY;
}

float Player::getForceX() {
	return forceX;
}

float Player::getForceY() {
	return forceY;
}

void Player::playFootstep()
{
	switch ((rand() % 4) + 1) {
	case 1: Mix_PlayChannel(-1, Step1, 0); break;
	case 2: Mix_PlayChannel(-1, Step2, 0); break;
	case 3: Mix_PlayChannel(-1, Step3, 0); break;
	case 4: Mix_PlayChannel(-1, Step4, 0); break;
	}

	lastStepX = mBox.x;
	lastStepY = mBox.y;
}

/*
tank::tank() //No-arg constructor
{
//Initialize the collision box
mBox.x = 0;
mBox.y = 0;
mBox.w = PLAYER_WIDTH;
mBox.h = PLAYER_HEIGHT;

//Initialize the velocity
vel = 0;
hullAngle = 0;
}

tank::tank(string name) //arg constructor
{
playerName = name;
//Initialize the collision box
mBox.x = 0;
mBox.y = 0;
mBox.w = PLAYER_WIDTH;
mBox.h = PLAYER_HEIGHT;

//Initialize the velocity
vel = 0;
hullAngle = 0;
}

tank::tank(int xCoord, int yCoord) //Constructor that takes position in x and y coords
{
playerXCoord = xCoord;
playerYCoord = yCoord;

//Initialize the collision box
mBox.x = 0;
mBox.y = 0;
mBox.w = PLAYER_WIDTH;
mBox.h = PLAYER_HEIGHT;

//Initialize the velocity
vel = 0;
hullAngle = 0;
}

tank::tank(int xCoord, int yCoord, string name) //Constructor that takes position in x and y coords
{
playerXCoord = xCoord;
playerYCoord = yCoord;

playerName = name;

//Initialize the collision box
mBox.x = 0;
mBox.y = 0;
mBox.w = PLAYER_WIDTH;
mBox.h = PLAYER_HEIGHT;

//Initialize the velocity
vel = 0;
hullAngle = 0;
}

void tank::handleEvent(SDL_Event& e)
{
//If a key was pressed
if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
{
//Adjust the velocity
switch (e.key.keysym.sym)
{
case SDLK_w: force += PLAYER_ACCEL * speedMultiplier; break;
case SDLK_s: force -= PLAYER_ACCEL * speedMultiplier; break;
case SDLK_a: turn -= 1; break;
case SDLK_d: turn += 1; break;
}
}
//If a key was released
else if (e.type == SDL_KEYUP && e.key.repeat == 0)
{
//Adjust the velocity
switch (e.key.keysym.sym)
{
case SDLK_w: force -= PLAYER_ACCEL * speedMultiplier; break;
case SDLK_s: force += PLAYER_ACCEL * speedMultiplier; break;
case SDLK_a: turn += 1; break;
case SDLK_d: turn -= 1; break;
}
}
}

void tank::move(Tile *tiles[])
{
hullAngle += turn * TURN_SPEED;
if (hullAngle > 360)
hullAngle = 0;
else if (hullAngle < 0)
hullAngle = 360;

vel += force;
if (vel > PLAYER_VEL)
vel = PLAYER_VEL;
if (vel < PLAYER_REV)
vel = PLAYER_REV;

if (force == 0.0 && vel > 0.0)
vel -= 0.05;
if (force == 0.0 && vel < 0.0)
vel += 0.05;

//Move the player left or right
setXCoord(getXCoord() + vel * cos(3.14159265 * hullAngle / 180.f));
mBox.x = getXCoord();

//If the player went too far to the left or right or touched a wall
if ((mBox.x < 0) || (mBox.x + PLAYER_WIDTH > LEVEL_WIDTH) || touchesWall(mBox, tiles))
{
//move back
setXCoord(getXCoord() - vel * cos(3.14159265 * hullAngle / 180.f));
mBox.x = getXCoord();
}

//Move the player up or down
setYCoord(getYCoord() + vel * sin(3.14159265 * hullAngle / 180.f));
mBox.y = getYCoord();

//If the player went too far up or down or touched a wall
if ((mBox.y < 0) || (mBox.y + PLAYER_HEIGHT > LEVEL_HEIGHT) || touchesWall(mBox, tiles))
{
//move back
setYCoord(getYCoord() - vel * sin(3.14159265 * hullAngle / 180.f));
mBox.y = getYCoord();
}
}

void tank::render(SDL_Rect& camera)
{
int xMouse;
int yMouse;

SDL_GetMouseState(&xMouse, &yMouse);
double degrees = atan2(yMouse - (getYCoord() + 10 - camera.y), xMouse - (getXCoord() + 10 - camera.x));
degrees = degrees * (180.0000 / 3.1415);

gPlayerMask.render(getXCoord() - camera.x - 2490, getYCoord() - camera.y - 2490, NULL, degrees);
gHullTexture.render(getXCoord() - camera.x, getYCoord() - camera.y, NULL, hullAngle);
gTurretTexture.render(getXCoord() - camera.x + (5 * cos(3.14159265 * degrees / 180.f)), getYCoord() - camera.y + (5 * sin(3.14159265 * degrees / 180.f)), NULL, degrees);

}
*/
powerUp::powerUp()
{

}

powerUp::powerUp(int number)
{
	choosePowerUp(number);

	//initialize powerup collision box
	pBox.x = 0;
	pBox.y = 0;
	pBox.w = POWERUP_WIDTH;
	pBox.h = POWERUP_HEIGHT;
}

int powerUp::generatePUpNum()
{
	int pUpNum = 0;
	//get number between 1 and 4
	pUpNum = (rand() % 4) + 1;
	return pUpNum;
}

void powerUp::choosePowerUp(int pUpNum)
{
	if (pUpNum == 1)
	{
		powerUpName = "Untaggable";
	}
	else if (pUpNum == 2)
	{
		powerUpName = "Super Speed";
	}
	else if (pUpNum == 3)
	{
		powerUpName = "True Sight";
	}
	else
	{
		powerUpName = "Invisible";
	}
}

//Buttons class definitions

Buttons::Buttons() {
	coordX = 10;
	coordY = 10;
	Bid = 1;
	setButtonTexture();
}

Buttons::Buttons(int bBid) {
	coordX = 10;
	coordY = 10;
	Bid = bBid;
	setButtonTexture();
}

int Buttons::getX() {
	return coordX;
}

int Buttons::getY() {
	return coordY;
}

void Buttons::setX(int x) {
	coordX = x;
}

void Buttons::setY(int y) {
	coordY = y;
}

void Buttons::setButtonTexture() {

	if (Bid == 1) {
		buttonSurface = IMG_Load("buttonPlay.png");
	}
	if (Bid == 2) {
		buttonSurface = IMG_Load("buttonHelp.png");
	}
	if (Bid == 3) {
		buttonSurface = IMG_Load("buttonQuit.png");
	}
	if (Bid == 4) {
		buttonSurface = IMG_Load("buttonBack.png");
	}
}

void Buttons::buttonRender() {
	SDL_Texture* buttonTexture = NULL;
	buttonTexture = SDL_CreateTextureFromSurface(gRenderer, buttonSurface);
	SDL_Rect renderQuad = { coordX, coordY, 200, 100 };
	SDL_RenderCopyEx(gRenderer, buttonTexture, NULL, &renderQuad, 0.0, NULL, SDL_FLIP_NONE);
}

bool Buttons::handleEvent(SDL_Event& e) {
	SDL_GetMouseState(&mousePosX, &mousePosY);
	//Check if mouse is in button
	bool inside = true;
	if (e.type == SDL_MOUSEBUTTONDOWN) {
		//mouse is outside button
		if (mousePosX < coordX || mousePosX >(coordX + 200) || mousePosY < coordY || mousePosY >(coordY + 150)) {
			inside = false;
			return true;
		}
		//mouse is inside button
		if (inside) {
			switch (e.type) {
			case SDL_MOUSEBUTTONDOWN:
				return false;
				break;
			}
		}
	}
}

int Buttons::onPress() {
	return 1;
}

bool init()
{
	//Initialization flag
	bool success = true;

	TTF_Init();

	//Initialize SDL_mixer
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		return false;
	}

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Tag 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadText() {
	bool success = true;

	//Open the font
	gFont = TTF_OpenFont("OpenSans-Regular.ttf", 28);
	if (gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	else
	{
		//Set text color as black
		SDL_Color textColor = { 0, 0, 0, 255 };
	}
	return success;
}

bool loadGameMedia(Tile* tiles[])
{
	//Loading success flag
	bool success = true;

	//Load dot texture
	if (!gPlayerTexture.loadFromFile("unnamed.png"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}

	/*	if (!gHullTexture.loadFromFile("TigerHull.png"))
	{
	printf("Failed to load dot texture!\n");
	success = false;
	}

	if (!gTurretTexture.loadFromFile("TigerTurret.png"))
	{
	printf("Failed to load dot texture!\n");
	success = false;
	}
	*/
	if (!gPlayerMask.loadFromFile("playerMaskAlt.png"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}

	//Load tile texture
	if (!gTileTexture.loadFromFile("tilesAltS.png"))
	{
		printf("Failed to load tile set texture!\n");
		success = false;
	}

	//Load tile map
	if (!setTiles(tiles))
	{
		printf("Failed to load tile set!\n");
		success = false;
	}

	//load menu stuff
	if (!mainMenuBackgroundTile.loadFromFile("backgroundTile.png")) {
		printf("Failed to load menu background!\n");
		success = false;
	}
	if (!gGameLogo.loadFromFile("logo.png")) {
		printf("Failed to load game's logo");
		success = false;
	}

	if (!gGameTutorial.loadFromFile("instructions.png")) {
		printf("Failed to load game tutorial");
		success = false;
	}

	//Load the music
	music = Mix_LoadMUS("pepeSong.wav");

	//If there was a problem loading the music
	if (music == NULL)
	{
		return false;
	}

	//Load sound effects
	tagSound = Mix_LoadWAV("tagSound.wav");
	gasBlast = Mix_LoadWAV("fartBeat.wav");
	Schlick = Mix_LoadWAV("Schlick.wav");
	Step1 = Mix_LoadWAV("step1.wav");
	Step2 = Mix_LoadWAV("step2.wav");
	Step3 = Mix_LoadWAV("step3.wav");
	Step4 = Mix_LoadWAV("step4.wav");

	return success;
}

void closeGameMedia(Tile* tiles[])
{
	//Deallocate tiles
	for (int i = 0; i < TOTAL_TILES; ++i)
	{
		if (tiles[i] == NULL)
		{
			delete tiles[i];
			tiles[i] = NULL;
		}
	}

	//Free loaded images
	gPlayerTexture.free();
	gTileTexture.free();
	gPlayerMask.free();
	//gHullTexture.free();
	//gTurretTexture.free();

	//Destroy window	
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

bool checkCollision(SDL_Rect a, SDL_Rect b)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//If any of the sides from A are outside of B
	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	//If none of the sides from A are outside B
	return true;
}

bool setTiles(Tile* tiles[])
{
	//Success flag
	bool tilesLoaded = true;

	//The tile offsets
	int x = 0, y = 0;

	//Open the map
	ifstream map("lazy.map");

	//If the map couldn't be loaded
	/*if( map == NULL )
	{
	printf( "Unable to load map file!\n" );
	tilesLoaded = false;
	}
	else*/

	{
		//Initialize the tiles
		for (int i = 0; i < TOTAL_TILES; ++i)
		{
			//Determines what kind of tile will be made
			int tileType = -1;

			//Read tile from map file
			map >> tileType;

			//If the was a problem in reading the map
			if (map.fail())
			{
				//Stop loading map
				printf("Error loading map: Unexpected end of file!\n");
				tilesLoaded = false;
				break;
			}

			//If the number is a valid tile number
			if ((tileType >= 0) && (tileType < TOTAL_TILE_SPRITES))
			{
				tiles[i] = new Tile(x, y, tileType);
			}
			//If we don't recognize the tile type
			else
			{
				//Stop loading map
				printf("Error loading map: Invalid tile type at %d!\n", i);
				tilesLoaded = false;
				break;
			}

			//Move to next tile spot
			x += TILE_WIDTH;

			//If we've gone too far
			if (x >= LEVEL_WIDTH)
			{
				//Move back
				x = 0;

				//Move to the next row
				y += TILE_HEIGHT;
			}
		}

		//Clip the sprite sheet
		if (tilesLoaded)
		{
			gTileClips[TILE_RED].x = 0;
			gTileClips[TILE_RED].y = 0;
			gTileClips[TILE_RED].w = TILE_WIDTH;
			gTileClips[TILE_RED].h = TILE_HEIGHT;

			gTileClips[TILE_GREEN].x = 0;
			gTileClips[TILE_GREEN].y = 80;
			gTileClips[TILE_GREEN].w = TILE_WIDTH;
			gTileClips[TILE_GREEN].h = TILE_HEIGHT;

			gTileClips[TILE_BLUE].x = 0;
			gTileClips[TILE_BLUE].y = 160;
			gTileClips[TILE_BLUE].w = TILE_WIDTH;
			gTileClips[TILE_BLUE].h = TILE_HEIGHT;

			gTileClips[TILE_TOPLEFT].x = 80;
			gTileClips[TILE_TOPLEFT].y = 0;
			gTileClips[TILE_TOPLEFT].w = TILE_WIDTH;
			gTileClips[TILE_TOPLEFT].h = TILE_HEIGHT;

			gTileClips[TILE_LEFT].x = 80;
			gTileClips[TILE_LEFT].y = 80;
			gTileClips[TILE_LEFT].w = TILE_WIDTH;
			gTileClips[TILE_LEFT].h = TILE_HEIGHT;

			gTileClips[TILE_BOTTOMLEFT].x = 80;
			gTileClips[TILE_BOTTOMLEFT].y = 160;
			gTileClips[TILE_BOTTOMLEFT].w = TILE_WIDTH;
			gTileClips[TILE_BOTTOMLEFT].h = TILE_HEIGHT;

			gTileClips[TILE_TOP].x = 160;
			gTileClips[TILE_TOP].y = 0;
			gTileClips[TILE_TOP].w = TILE_WIDTH;
			gTileClips[TILE_TOP].h = TILE_HEIGHT;

			gTileClips[TILE_CENTER].x = 160;
			gTileClips[TILE_CENTER].y = 80;
			gTileClips[TILE_CENTER].w = TILE_WIDTH;
			gTileClips[TILE_CENTER].h = TILE_HEIGHT;

			gTileClips[TILE_BOTTOM].x = 160;
			gTileClips[TILE_BOTTOM].y = 160;
			gTileClips[TILE_BOTTOM].w = TILE_WIDTH;
			gTileClips[TILE_BOTTOM].h = TILE_HEIGHT;

			gTileClips[TILE_TOPRIGHT].x = 240;
			gTileClips[TILE_TOPRIGHT].y = 0;
			gTileClips[TILE_TOPRIGHT].w = TILE_WIDTH;
			gTileClips[TILE_TOPRIGHT].h = TILE_HEIGHT;

			gTileClips[TILE_RIGHT].x = 240;
			gTileClips[TILE_RIGHT].y = 80;
			gTileClips[TILE_RIGHT].w = TILE_WIDTH;
			gTileClips[TILE_RIGHT].h = TILE_HEIGHT;

			gTileClips[TILE_BOTTOMRIGHT].x = 240;
			gTileClips[TILE_BOTTOMRIGHT].y = 160;
			gTileClips[TILE_BOTTOMRIGHT].w = TILE_WIDTH;
			gTileClips[TILE_BOTTOMRIGHT].h = TILE_HEIGHT;
		}
	}

	//Close the file
	map.close();

	//If the map was loaded fine
	return tilesLoaded;
}

bool touchesWall(SDL_Rect box, Tile* tiles[])
{
	//Go through the tiles
	for (int i = 0; i < TOTAL_TILES; ++i)
	{
		//If the tile is a wall type tile
		if ((tiles[i]->getType() >= TILE_CENTER) && (tiles[i]->getType() <= TILE_TOPLEFT))
		{
			//If the collision box touches the wall tile
			if (checkCollision(box, tiles[i]->getBox()))
			{
				return true;
			}
		}
	}

	//If no wall tiles were touched
	return false;
}

bool touchesPlayer(SDL_Rect box, Timer& currentTimer, int testPlayer)
{
	//Go through the other players
	for (int i = 0; i < currentTimer.players.size(); ++i)
	{
		if (i == testPlayer) {
			continue;
		}

		//If the collision box touches another player
		if (checkCollision(box, currentTimer.players[i].getBox()))
		{
			if ((SDL_GetTicks() - currentTimer.getLastTagTime()) > 500 && currentTimer.players[testPlayer].getIt() && currentTimer.getRunning())
			{
				Mix_PlayChannel(-1, tagSound, 0);
				currentTimer.newTag(currentTimer.players[i].getPlayerName());
			}
			return true;
		}

	}

	//If no players were touched
	return false;
}


void gameLoop() {
	//The level tiles
	Tile* tileSet[TOTAL_TILES];

	//Load media
	if (!loadGameMedia(tileSet))
	{
		printf("Failed to load media!\n");
	}

	//Main loop flag
	bool quit = false;

	//menu stuff
	bool pause = false;
	Buttons pauseQuit(3);

	//Event handler
	SDL_Event e;

	Timer testTimer(gWindow, gRenderer);
	testTimer.setTimeLimit(900000);
	testTimer.addPlayer("Player 1");
	testTimer.players[0].setXCoord(80);
	testTimer.players[0].setYCoord(80);

	testTimer.addPlayer("Player 2");
	testTimer.players[1].setXCoord(120);
	testTimer.players[1].setYCoord(40);

	for (int i = 0; i < testTimer.players.size(); i++) {
		testTimer.players[i].setPID(i);
	}

	playerAI player2AI(&testTimer.players, 1);

	//Level camera
	SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

	testTimer.startTimer();

	testTimer.newTag("Player 2");

	//While application is running
	do {
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			if (e.type == SDL_KEYDOWN)
			{
				if (e.key.keysym.sym == SDLK_m)
				{
					//If there is no music playing
					if (Mix_PlayingMusic() == 0)
					{
						//Play the music
						Mix_PlayMusic(music, -1);
					}
					//If music is being played
					else
					{
						//If the music is paused
						if (Mix_PausedMusic() == 1)
						{
							//Resume the music
							Mix_ResumeMusic();
						}
						//If the music is playing
						else
						{
							//Pause the music
							Mix_PauseMusic();
						}
					}
				}
			}

			testTimer.players[0].handleEvent(e);
			testTimer.handleEvent(e);

		}

		testTimer.players[0].move(tileSet, testTimer);
		testTimer.players[1].move(tileSet, testTimer);

		testTimer.players[0].setCamera(camera);

		//Clear screen
		SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(gRenderer);

		//Render level
		for (int i = 0; i < TOTAL_TILES; ++i) {
			tileSet[i]->render(camera);
		}

		if (testTimer.players.size() > 1) {
			struct pair {
				int ID;
				int distance;
				pair(int newID, int newDist) {
					ID = newID;
					distance = newDist;
				}
			};

			vector<pair> playersByDistance;

			for (int i = 1; i < testTimer.players.size(); i++) {
				int x = (testTimer.players[i].getXCoord() - camera.x + 10) - (testTimer.players[0].getXCoord() - camera.x + 10);
				x *= (testTimer.players[i].getXCoord() - camera.x + 10) - (testTimer.players[0].getXCoord() - camera.x + 10);
				int y = (testTimer.players[i].getYCoord() - camera.y + 10) - (testTimer.players[0].getYCoord() - camera.y + 10);
				y *= (testTimer.players[i].getYCoord() - camera.y + 10) - (testTimer.players[0].getYCoord() - camera.y + 10);
				int dist = sqrt(x + y);

				if (playersByDistance.size() == 0) {
					playersByDistance.push_back(pair(i, dist));
				}
				else {
					for (int j = 0; j < playersByDistance.size(); j++) {
						if (dist < playersByDistance[j].distance) {
							playersByDistance.emplace(playersByDistance.begin() + j, pair(i, dist));
							break;
						}
						else if (j == playersByDistance.size() - 1) {
							playersByDistance.push_back(pair(i, dist));
							break;
						}
					}
				}
			}

			for (int i = playersByDistance.size() - 1; i >= 0; i--) {
				double directAngle = (180.f / 3.14159265f) * atan2((int)(testTimer.players[playersByDistance[i].ID].getBox().y - camera.y) - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)(testTimer.players[playersByDistance[i].ID].getBox().x - camera.x)) - (testTimer.players[0].getXCoord() + 10 - camera.x));
				double oppAngle = 0.0;
				double otherAngle = 0.0;

				oppAngle = directAngle + 90;
				if (oppAngle > 180)
					oppAngle -= 360;

				if (playersByDistance[i].distance < 400) {
					Sint16 player1x = testTimer.players[playersByDistance[i].ID].getXCoord() + 10 + (7 * cos(oppAngle * (3.14159265f / 180.f)) - camera.x);
					Sint16 player1y = testTimer.players[playersByDistance[i].ID].getYCoord() + 10 + (7 * sin(oppAngle * (3.14159265f / 180.f)) - camera.y);
					double angle1 = (180.f / 3.14159265f) * atan2(((int)player1y - camera.y) - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)player1x - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
					Sint16 wall1x = player1x + 500 * cos(3.14159265f * angle1 / 180.f);
					Sint16 wall1y = player1y + 500 * sin(3.14159265f * angle1 / 180.f);

					Sint16 player2x = testTimer.players[playersByDistance[i].ID].getXCoord() + 10 + (-7 * cos(oppAngle * (3.14159265f / 180.f)) - camera.x);
					Sint16 player2y = testTimer.players[playersByDistance[i].ID].getYCoord() + 10 + (-7 * sin(oppAngle * (3.14159265f / 180.f)) - camera.y);
					double angle2 = (180.f / 3.14159265f) * atan2(((int)player2y - camera.y) - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)player2x - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
					Sint16 wall2x = player2x + 500 * cos(3.14159265f * angle2 / 180.f);
					Sint16 wall2y = player2y + 500 * sin(3.14159265f * angle2 / 180.f);

					Sint16 pxArray1[4] = { player1x, player2x, wall2x, wall1x };
					Sint16 pyArray1[4] = { player1y, player2y, wall2y, wall1y };
					filledPolygonRGBA(gRenderer, pxArray1, pyArray1, 4, 0, 0, 0, 255);

					switch (playersByDistance[i].ID) {
					case 1: gPlayerTexture.setColor(255, 150, 150); break;
					case 2: gPlayerTexture.setColor(150, 150, 255); break;
					case 3: gPlayerTexture.setColor(255, 255, 150); break;
					case 4: gPlayerTexture.setColor(150, 255, 150); break;
					case 5: gPlayerTexture.setColor(150, 150, 150); break;
					case 6: gPlayerTexture.setColor(75, 75, 75); break;
					case 7: gPlayerTexture.setColor(255, 150, 255); break;
					}

					if (testTimer.players[playersByDistance[i].ID].getIt()) {
						filledCircleRGBA(gRenderer, testTimer.players[playersByDistance[i].ID].getXCoord() + 10 - camera.x, testTimer.players[playersByDistance[i].ID].getYCoord() + 10 - camera.y, 20, 255, 0, 0, 128);
					}

					testTimer.players[playersByDistance[i].ID].render(camera);
				}
			}
		}

		for (int i = 0; i < TOTAL_TILES; i++) {
			if ((tileSet[i]->getType() >= TILE_TOP) && (tileSet[i]->getType() <= TILE_TOPLEFT)) {
				int x = (tileSet[i]->getBox().x - camera.x) - (testTimer.players[0].getXCoord() - camera.x);
				x *= (tileSet[i]->getBox().x - camera.x) - (testTimer.players[0].getXCoord() - camera.x);
				int y = (tileSet[i]->getBox().y - camera.y) - (testTimer.players[0].getYCoord() - camera.y);
				y *= (tileSet[i]->getBox().y - camera.y) - (testTimer.players[0].getYCoord() - camera.y);
				int distance = sqrt(x + y);

				if (distance < 475) {
					bool needTL = false;
					bool needTR = false;
					bool needBR = false;
					bool needBL = false;
					double angleTL = 0;
					double angleTR = 0;
					double angleBR = 0;
					double angleBL = 0;
					Sint16 boxTLx = 0;
					Sint16 boxTLy = 0;
					Sint16 boxTRx = 0;
					Sint16 boxTRy = 0;
					Sint16 boxBRx = 0;
					Sint16 boxBRy = 0;
					Sint16 boxBLx = 0;
					Sint16 boxBLy = 0;
					Sint16 wallTLx = 0;
					Sint16 wallTLy = 0;
					Sint16 wallTRx = 0;
					Sint16 wallTRy = 0;
					Sint16 wallBRx = 0;
					Sint16 wallBRy = 0;
					Sint16 wallBLx = 0;
					Sint16 wallBLy = 0;

					int tileUpType = -1;
					int tileLeftType = -1;
					int tileRightType = -1;
					int tileDownType = -1;

					if (i < 16)
						tileUpType = 4;
					else
						tileUpType = tileSet[i - 16]->getType();
					if (i == 0)
						tileLeftType = 4;
					else
						tileLeftType = tileSet[i - 1]->getType();
					if (i > TOTAL_TILES - 16)
						tileDownType = 4;
					else
						tileDownType = tileSet[i + 16]->getType();
					if (i == TOTAL_TILES)
						tileRightType = 4;
					else
						tileRightType = tileSet[i + 1]->getType();

					boxTLx = tileSet[i]->getBox().x - camera.x;
					boxTLy = tileSet[i]->getBox().y - camera.y;
					boxBRx = tileSet[i]->getBox().x + tileSet[i]->getBox().w - camera.x;
					boxBRy = tileSet[i]->getBox().y + tileSet[i]->getBox().h - camera.y;

					if (((testTimer.players[0].getXCoord() + 10 - camera.x) < boxTLx) && !(tileLeftType >= 3)) {
						needTL = true;
						needBL = true;
					}
					if (((testTimer.players[0].getXCoord() + 10 - camera.x) > boxBRx) && !(tileRightType >= 3)) {
						needTR = true;
						needBR = true;
					}
					if (((testTimer.players[0].getYCoord() + 10 - camera.y) < boxTLy) && !(tileUpType >= 3)) {
						needTL = true;
						needTR = true;
					}
					if (((testTimer.players[0].getYCoord() + 10 - camera.y) > boxBRy) && !(tileDownType >= 3)) {
						needBL = true;
						needBR = true;
					}

					int shadowLength = 2000;

					if (needTL) {
						angleTL = (180.f / 3.14159265f) * atan2((int)boxTLy - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)boxTLx - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
						wallTLx = boxTLx + shadowLength * cos(3.14159265f * angleTL / 180.f);
						boxTLx = boxTLx + 15 * cos(3.14159265f * angleTL / 180.f);
						wallTLy = boxTLy + shadowLength * sin(3.14159265f * angleTL / 180.f);
						boxTLy = boxTLy + 15 * sin(3.14159265f * angleTL / 180.f);
					}

					if (needBR) {
						angleBR = (180.f / 3.14159265f) * atan2((int)boxBRy - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)boxBRx - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
						wallBRx = boxBRx + shadowLength * cos(3.14159265f * angleBR / 180.f);
						boxBRx = boxBRx + 15 * cos(3.14159265f * angleBR / 180.f);
						wallBRy = boxBRy + shadowLength * sin(3.14159265f * angleBR / 180.f);
						boxBRy = boxBRy + 15 * sin(3.14159265f * angleBR / 180.f);
					}

					if (needTR) {
						boxTRx = tileSet[i]->getBox().x + tileSet[i]->getBox().w - camera.x;
						boxTRy = tileSet[i]->getBox().y - camera.y;
						angleTR = (180.f / 3.14159265f) * atan2((int)boxTRy - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)boxTRx - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
						wallTRx = boxTRx + shadowLength * cos(3.14159265f * angleTR / 180.f);
						boxTRx = boxTRx + 15 * cos(3.14159265f * angleTR / 180.f);
						wallTRy = boxTRy + shadowLength * sin(3.14159265f * angleTR / 180.f);
						boxTRy = boxTRy + 15 * sin(3.14159265f * angleTR / 180.f);
					}
					if (needBL) {
						boxBLx = tileSet[i]->getBox().x - camera.x;
						boxBLy = tileSet[i]->getBox().y + tileSet[i]->getBox().h - camera.y;
						angleBL = (180.f / 3.14159265f) * atan2((int)boxBLy - (testTimer.players[0].getYCoord() + 10 - camera.y), ((int)boxBLx - camera.x) - (testTimer.players[0].getXCoord() + 10 - camera.x));
						wallBLx = boxBLx + shadowLength * cos(3.14159265f * angleBL / 180.f);
						boxBLx = boxBLx + 15 * cos(3.14159265f * angleBL / 180.f);
						wallBLy = boxBLy + shadowLength * sin(3.14159265f * angleBL / 180.f);
						boxBLy = boxBLy + 15 * sin(3.14159265f * angleBL / 180.f);
					}

					if (needTL && needTR) {
						Sint16 xArray1[4] = { boxTLx, boxTRx, wallTRx, wallTLx };
						Sint16 yArray1[4] = { boxTLy, boxTRy, wallTRy, wallTLy };
						filledPolygonRGBA(gRenderer, xArray1, yArray1, 4, 0, 0, 0, 255);
					}

					if (needTR && needBR) {
						Sint16 xArray2[4] = { boxTRx, boxBRx, wallBRx, wallTRx };
						Sint16 yArray2[4] = { boxTRy, boxBRy, wallBRy, wallTRy };
						filledPolygonRGBA(gRenderer, xArray2, yArray2, 4, 0, 0, 0, 255);
					}

					if (needBR && needBL) {
						Sint16 xArray3[4] = { boxBRx, boxBLx, wallBLx, wallBRx };
						Sint16 yArray3[4] = { boxBRy, boxBLy, wallBLy, wallBRy };
						filledPolygonRGBA(gRenderer, xArray3, yArray3, 4, 0, 0, 0, 255);
					}

					if (needBL && needTL) {
						Sint16 xArray4[4] = { boxBLx, boxTLx, wallTLx, wallBLx };
						Sint16 yArray4[4] = { boxBLy, boxTLy, wallTLy, wallBLy };
						filledPolygonRGBA(gRenderer, xArray4, yArray4, 4, 0, 0, 0, 255);
					}
				}
			}
		}

		if (testTimer.players[0].getIt()) {
			filledCircleRGBA(gRenderer, testTimer.players[0].getXCoord() + 10 - camera.x, testTimer.players[0].getYCoord() + 10 - camera.y, 20, 255, 0, 0, 128);
		}
		gPlayerTexture.setColor(255, 255, 255);
		testTimer.players[0].render(camera);

		player2AI.think();
		player2AI.processAI();

		testTimer.updateTimer();

		//Update screen
		SDL_RenderPresent(gRenderer);

	} while (!quit);

	closeGameMedia(tileSet);
	testTimer.kill();
}

int main(int argc, char* args[])
{
	//create loop variables
	bool masterLoop = true;
	bool mainMenu = true;
	bool mainGame = false;
	bool secondMenu = false;
	bool quit = false;
	bool pause = false;
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

		//this god damn section of code caused me an hour of pain
		Tile* tileSet[TOTAL_TILES];
		if (!loadGameMedia(tileSet))
		{
			printf("Failed to load media!\n");
		}
		//
		
		//create buttons
		Buttons menuPlay(1);
		Buttons menuHelp(2);
		Buttons menuQuit(3);
		Buttons menuBack(4);
		Buttons pauseQuit(3);
		
		while (masterLoop) {
			SDL_Event e;
			//enter main menu loop
			while (mainMenu) {
				//place buttons
				menuPlay.setX((SCREEN_WIDTH / 2) - 100);
				menuPlay.setY(300);

				menuHelp.setX((SCREEN_WIDTH / 2) - 100);
				menuHelp.setY(450);

				menuQuit.setX((SCREEN_WIDTH / 2) - 100);
				menuQuit.setY(600);

				menuBack.setX((SCREEN_WIDTH / 2) - 100);
				menuBack.setY(600);

				//check for mouse clicks
				while (SDL_PollEvent(&e) != 0) {
					if (e.type == SDL_QUIT) {
						quit = true;
						break;
					}
					else if (e.type == SDL_MOUSEBUTTONDOWN) {
						if (!menuPlay.handleEvent(e)) {
							mainMenu = false;
							secondMenu = false;
							mainGame = true;
						}
						if (!menuHelp.handleEvent(e)) {
							secondMenu = true;
							mainMenu = false;
							mainGame = false;
						}
						if (!menuQuit.handleEvent(e)) {
							mainGame = false;
							mainMenu = false;
							masterLoop = false;
							break;
						}
					}
				}
				SDL_RenderClear(gRenderer);
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//render background
				
				for (int i = 0; i < SCREEN_WIDTH; i += mainMenuBackgroundTile.getWidth()) {
					for (int j = 0; j < SCREEN_HEIGHT; j += mainMenuBackgroundTile.getHeight()) {
						mainMenuBackgroundTile.render(i, j);
					}
				}
				
				

				//render buttons
				menuPlay.buttonRender();
				menuHelp.buttonRender();
				menuQuit.buttonRender();
				gGameLogo.render((SCREEN_WIDTH / 2) - (gGameLogo.getWidth() / 2), 40);
				SDL_RenderPresent(gRenderer);

				
			}//end main menu loop

			//tutorial menu
			while (secondMenu) {
				while (SDL_PollEvent(&e) != 0) {
					if (e.type == SDL_QUIT) {
						quit = true;
					}
					else if (e.type == SDL_MOUSEBUTTONDOWN) {
						if (!menuBack.handleEvent(e)) {
							mainMenu = true;
							secondMenu = false;
							mainGame = false;
							masterLoop = true;
						}
					}
				}

				SDL_RenderClear(gRenderer);
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				//render background image
				for (int i = 0; i < SCREEN_WIDTH; i += mainMenuBackgroundTile.getWidth()) {
					for (int j = 0; j < SCREEN_HEIGHT; j += mainMenuBackgroundTile.getHeight()) {
						mainMenuBackgroundTile.render(i, j);
					}
				}
				

				menuBack.buttonRender();
				gGameTutorial.render((SCREEN_WIDTH / 2) - (gGameLogo.getWidth() / 2), 40);
				SDL_RenderPresent(gRenderer);
			}

			while (mainGame) {
				loadText();
				gameLoop();
				//Free resources and close SDL

				//check for escape press and render button if paused
				while (SDL_PollEvent(&e) != 0) {
					if (e.type == SDL_KEYDOWN) {
						if (e.key.keysym.sym == SDLK_ESCAPE) {
							if (pause) {
								pause = false;
							}
							else if (!pause) {
								pause = true;
							}
						}
					}

					if (e.type == SDL_MOUSEBUTTONDOWN) {
						if (pause) {
							if (!pauseQuit.handleEvent(e)) {
								quit = true;
								mainGame = false;
								masterLoop = false;
							}
						}
					}
				}

				if (pause) {
					pauseQuit.setX(SCREEN_WIDTH - 200);
					pauseQuit.setY((SCREEN_HEIGHT / 2) - 100);
					pauseQuit.buttonRender();
				}
			}
		}
	}

	return 0;
}